require 'rails_helper'

RSpec.describe Tweet, type: :model do
  describe 'body' do
    describe 'validations' do
      describe 'same_tweet?' do
        let(:text1) { 'text 1' }
        let(:user) { FactoryBot.create(:user) }
        let!(:tweet1) { FactoryBot.create(:tweet, user: user, body: text1) }
        let(:tweet2) { FactoryBot.build(:tweet, user: user, body: text1) }

        it 'validates that the same tweet can\'t be created in the last 24hrs' do
          expect(tweet2.valid?).to be false
        end
      end
    end
  end
end
