class HomeController < ApplicationController
  TWEETS_PER_PAGE = Api::TweetsController::TWEETS_PER_PAGE

  def index
    @tweets = Tweet.latest.limit(TWEETS_PER_PAGE).offset(offset)
  end
end
