class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :username])
  end

  private

  def page_params
    params.permit(:page).merge(per_page: TWEETS_PER_PAGE)
  end

  def page
    params[:page]
  end

  def offset
    return 0 if !page || page == 1

    page * (page.to_i - 1)
  end
end
