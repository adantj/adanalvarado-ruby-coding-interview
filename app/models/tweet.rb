class Tweet < ApplicationRecord
  belongs_to :user

  validates :body, length: { maximum: 180 }
  validate :same_tweet?

  scope :by_date, ->(start_date, end_date, user_id) do
    Tweet
      .where(created_at: start_date...end_date)
      .where(user_id:)
  end

  scope :latest, -> { where(nil).order(created_at: :desc).group(:user_id) }

  def same_tweet?
    if self
       .class
       .by_date(1.day.ago, Time.zone.now, user_id)
       .where(body:)
       .any?

      errors.add(:body, "Can't be the same tweet in 24 hours")
    end
  end
end
